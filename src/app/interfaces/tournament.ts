export interface Tournament {
    name:string;
    beginDate:string;
    status:string;
    results:string;
    opponents:string;
    lolVersion:string;
    numberOfGames:number;
    games:string;
}
