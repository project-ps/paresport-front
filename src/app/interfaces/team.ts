export interface Team {
    name:string;
    teamAcronym:string;
    teamLogo:string;
    playerRealName:string;
    playerCurrentName:string;
}
