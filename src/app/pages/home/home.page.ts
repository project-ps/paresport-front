import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  sliderConfig = {
    centeredSlides: false,
    spaceBetween: 7,
    freeMode:true,
  };

  public filters = [
    {
      title: 'Tendances',
      checked: true,
    },
    {
      title: 'CS-GO',
      checked: false,
    },
    {
      title: 'Overwatch',
      checked: false,
    },
    {
      title: 'Leagues of legend',
      checked: false,
    },
    {
      title: 'Dota 2',
      checked: false,
    },
  ];

  //DATA ARRAY
  public matches = [
    {
      name:"X Bet Rampage Series 5",
      game:"Dota 2",
      firstTeam:{
        logo:"teamsingularity.png",
        name:"TEAM SINGULARITY",
      },
      secondTeam: {
        logo:"thefinaltribe.png",
        name:"THE FINAL TRIBE",
      },
      date:"15 Mai",
      hour:"13:01"
    },
    {
      name:"ESL Pro League 9 APAC",
      game:"CS-GO",
      firstTeam:{
        logo:"luciddream.png",
        name:"LUCID DREAM",
      },
      secondTeam: {
        logo:"tyloo.png",
        name:"TYLOO",
      },
      date:"16 Mai",
      hour:"12:02"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
