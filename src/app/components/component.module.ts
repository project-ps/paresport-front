import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { TokenBadgeComponent } from './token-badge/token-badge.component';
import { DualCardComponent } from './dual-card/dual-card.component';

@NgModule({
  declarations: [
    //Call component
    HeaderComponent,
    TokenBadgeComponent,
    DualCardComponent,
  ],
  exports: [
    //Export component can be show in pages
    HeaderComponent,
    TokenBadgeComponent,
    DualCardComponent,
  ],
  imports: [
    //Default, don't touch !!
    IonicModule
  ]
})
export class ComponentsModule {}