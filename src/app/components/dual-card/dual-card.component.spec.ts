import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DualCardComponent } from './dual-card.component';

describe('DualCardComponent', () => {
  let component: DualCardComponent;
  let fixture: ComponentFixture<DualCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DualCardComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DualCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
