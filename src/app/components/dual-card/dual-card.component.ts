import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'dual-card',
  templateUrl: './dual-card.component.html',
  styleUrls: ['./dual-card.component.scss'],
})
export class DualCardComponent implements OnInit {

  @Input() color: string = '#c776d7';
  @Input() gameLogo: string = 'default.png';
  @Input() tournamentName: string = 'Aucun Tournoi';
  @Input() teamOneLogo: string;
  @Input() teamTwoLogo: string;
  @Input() teamOneName: string;
  @Input() teamTwoName: string;
  @Input() tournamentDate: string = 'À venir';
  @Input() tournamentHour: string;

  constructor() { }

  ngOnInit() {}

}
