import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'token-badge',
  templateUrl: './token-badge.component.html',
  styleUrls: ['./token-badge.component.scss'],
})
export class TokenBadgeComponent implements OnInit {

  @Input() value: string = '0';

  constructor() { }

  ngOnInit() {}

}
