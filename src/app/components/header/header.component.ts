import { Component, OnInit, Input} from '@angular/core';
import { CloneVisitor } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'default-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() titleSmall:string = "Par'e";
  @Input() titleStrong:string = "Sport";

  constructor() { }

  ngOnInit() {
  }

}
