import { Injectable } from '@angular/core';
import { Team } from 'src/app/interfaces/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  team:Array<Team>;

  constructor() { 

  }
}
