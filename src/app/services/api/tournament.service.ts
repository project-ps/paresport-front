import { Injectable } from '@angular/core';
import { Tournament } from 'src/app/interfaces/tournament';

@Injectable({
  providedIn: 'root'
})
export class TournamentService {

  tournaments:Array<Tournament>;

  constructor() { }
}
